============
ID14 project
============

[![build status](https://gitlab.esrf.fr/ID14/id14/badges/master/build.svg)](http://ID14.gitlab-pages.esrf.fr/ID14)
[![coverage report](https://gitlab.esrf.fr/ID14/id14/badges/master/coverage.svg)](http://ID14.gitlab-pages.esrf.fr/id14/htmlcov)

ID14 software & configuration

Latest documentation from master can be found [here](http://ID14.gitlab-pages.esrf.fr/id14)
