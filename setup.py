# -*- coding: utf-8 -*-

"""The setup script."""

import sys
from setuptools import setup, find_packages

TESTING = any(x in sys.argv for x in ['test', 'pytest'])

with open('README.rst') as readme_file:
    readme = readme_file.read()

requirements = ['bliss']

setup_requirements = ['pytest-runner', 'pytest'] if TESTING else []

test_requirements = ['pytest-cov', 'mock']

setup(
    author="BCU Team",
    author_email='bliss@esrf.fr',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    description="ID14 software & configuration",
    install_requires=requirements,
    license="GNU Lesser General Public License v3",
    long_description=readme,
    include_package_data=True,
    keywords='id14',
    name='id14',
    packages=find_packages(include=['id14']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.esrf.fr/ID14/id14',
    version='0.1.0',
    zip_safe=False,
)
