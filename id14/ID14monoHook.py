
import click
import gevent
import time
import numpy
import random
import tabulate

from bliss.common.hook import MotionHook
from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD
from bliss import setup_globals
from bliss.controllers.motor import CalcController
from bliss.config.settings import QueueSetting, ParametersWardrobe
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss import setup_globals, current_session
from bliss.config.static import get_config
from bliss.common.logtools import *

class HHLMonoCheckHook(MotionHook):
    """
    ID18/ID14 High Heat Load kohzu monochromator protection hook:
        .. motors monu and montrav should not move together
        .. motors mond and montrav should not move together
        .. position difference between monu and mond before and after a movement should not exceed MONU_MOND_MAX_ANGLE degrees.
        .. formerly, in spec, the average value between the two (mon[ud]) encoders was considered. the difference
        between mond and that value, should not exceed max_angle.
        .. position of motor montrav should not exceed montrav_adaptive_value before of after movement.
    Formerly in spec, posdiff was .5, max_angle was .45, montrav_adaptive_value was .67
    """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        super().__init__()
        
        # Get motors
        self._motors = {}
        for role in ["monu", "mond", "montrav"] :
            mot = config.get(role, None)
            if mot is None:
                raise RuntimeError(f"No \"{role}\" motor defined")
            self._motors[role] = mot

        # largest allowed difference between monu and mond
        self._max_angle = self.config.get('monu_mond_max_angle', None)
        if self._max_angle is None:
            raise RuntimeError(f"monu_mond_max_angle not defined")
        
        # Magic number between 0.5 and 0.8
        self._montrav_adaptive_value = self.config.get('montrav_adaptive_value', None)
        if self._montrav_adaptive_value is None:
            raise RuntimeError(f"montrav_adaptive_value not defined")
        
        # Offset of the reflected beam to the incoming beam [mm]
        self._beam_offset = self.config.get('beam_offset')
        if self._beam_offset is None:
            raise RuntimeError(f"beam_offset not defined")
        
        # if monu and mond positions are below "min_check", movement is allowed
        self._min_check = self.config.get('min_check')
        if self._min_check is None:
            raise RuntimeError(f"min_check not defined")
        
        # mond position/encoder macimum discrepancy
        self._mond_discrepancy = self.config.get('mond_discrepancy')
        if self._mond_discrepancy is None:
            raise RuntimeError(f"_mond_discrepancy not defined")
            
        self._do_check = True
        
    def __info__(self):
        
        ret_str = "\n"
        ret_str += f"HHLMonoCheckHook {self.name} - class for High Heat Load Monochromator safety.\n\n"
        ret_str += "MOTORS\n"
        ret_str += "Upstream cristal          : " + self._motors["monu"] + "\n"
        ret_str += "Downstream cristal        : " + self._motors["mond"] + "\n"
        ret_str += "Traversal movement        : " + self._motors["montrav"] + "\n"
        ret_str += "\n"
        ret_str += "PARAMETERS\n"
        ret_str += f"monu/mond max angle       : {self._max_angle}\n"
        ret_str += f"montrav adaptive value    : {self._montrav_adaptive_value}\n"
        ret_str += f"Beam offset               : {self._beam_offset}\n"
        ret_str += f"Minimum checked Movement  : {self._min_check}\n"
        ret_str += f"mond position/encoder maximum discrepancy  : {self._mond_discrepancy}\n"
        
        return ret_str

    def pre_move(self, motion_list):
        self._motion_list = motion_list
        
        log_debug(self, "----------------> premove")
        if self._do_check:
            self.kohzu_check(motion_list)

    def post_move(self, motion_list):        
        if self._do_check:
            p_str = "\n" + self._get_info_str() + "\n\n\n"
            print(p_str)

    def _get_info_str(self):
        lines = [["", "monu", "mond", "montrav"]]
        
        line1 = ["Motor"]
        line2 = ["Encoder"]
        
        for mot in self._motors.values():
            pos = get_config().get(mot).position
            line1.append(GREEN(pos))
            try:
                encoder = get_config().get(f"{mot}_enc")
                pos_enc = encoder.read()
                if numpy.fabs(pos-pos_enc) < 0.002:
                    line2.append(GREEN(f"{pos_enc:.3f}"))
                else:
                    line2.append(RED(f"{pos_enc:.3f}"))
            except:
                line2.append("NO ENC")
                
        lines.append(line1)
        lines.append(line2)
        
        p_str = "\n"+tabulate.tabulate(lines, tablefmt="plain")+"\n\n\n"
        
        return p_str
        
    def kohzu_check(self, motion_list):
        
        log_debug(self, "motion_list: {len(motion_list)} elements.")
        mnames = [i.axis.name for i in motion_list]
        comprehensive_motion_list = dict(zip(mnames, motion_list))
        log_debug(self, mnames)

        # monu, mond and montrav shouldn't move simultaneously
        if self._motors["monu"] in mnames and self._motors["mond"] in mnames and self._motors["montrav"] in mnames:
            raise RuntimeError(RED("\"monu\", \"mond\" and \"montrav\" motors can\'t move simultaneously!"))

        # monu and montrav shouldn't move simultaneously
        if self._motors["monu"] in mnames and self._motors["montrav"] in mnames:
            raise RuntimeError(RED("\"monu\" and \"montrav\" motors can\'t move simultaneously!"))

        # mond and montrav shouldn't move simultaneously
        if self._motors["mond"] in mnames and self._motors["montrav"] in mnames:
            raise RuntimeError(RED("\"mond\" and \"montrav\" motors can\'t move simultaneously!"))

        curr_mond = get_config().get(self._motors["mond"]).position
        if self._motors["mond"] in mnames:
            target_mond = comprehensive_motion_list[self._motors["mond"]].user_target_pos
        else:
            target_mond = curr_mond
            
        mond_name = self._motors["mond"]
        if numpy.fabs(curr_mond - get_config().get(f"{mond_name}_enc").read()) > self._mond_discrepancy:
            raise RuntimeError(RED(f"\"mond\" position/encoder discrepancy > {self._mond_discrepancy}"))
            
        curr_monu = get_config().get(self._motors["monu"]).position
        if self._motors["monu"] in mnames:
            target_monu = comprehensive_motion_list[self._motors["monu"]].user_target_pos
        else:
            target_monu = curr_monu
           
        if not self._is_valid_position(motion_list, curr_monu, target_monu, curr_mond, target_mond):
            raise RuntimeError(self._error)
            

    def _is_valid_position(self, motion_list, curr_monu, target_monu, curr_mond, target_mond):

        mnames = [i.axis.name for i in motion_list]
        comprehensive_motion_list = dict(zip(mnames, motion_list))
        
        monu_mond_max = numpy.maximum(curr_monu, target_monu)
        monu_mond_max = numpy.maximum(monu_mond_max, curr_mond)
        monu_mond_max = numpy.maximum(monu_mond_max, target_mond)
        
        if monu_mond_max > self._min_check:
            
            # monu AND mond is moving
            if self._motors["monu"] in mnames and self._motors["mond"] in mnames:
                if numpy.fabs(curr_monu -  target_monu) > self._max_angle:
                    self._error = RED(f"Moving both \"monu\" and \"mond\" and \"monu\" more than {self._max_angle}")
                    return False
                if numpy.fabs(curr_mond -  target_mond) > self._max_angle:
                    self._error = RED(f"Moving both \"monu\" and \"mond\" and \"mond\" more than {self._max_angle}")
                    return False
            else:
                # monu is moving alone
                if self._motors["monu"] in mnames:
                    if numpy.fabs(target_monu - curr_mond) > self._max_angle:
                        self._error = RED(f"Motor \"monu\" and \"mond\" more than {self._max_angle} degree apart")
                        return False
                # mond is moving alone
                if self._motors["mond"] in mnames:
                    if numpy.fabs(target_mond - curr_monu) > self._max_angle:
                        self._error = RED(f"Motor \"monu\" and \"mond\" more than {self._max_angle} degree apart")
                        return False
            
            if self._motors["montrav"] in mnames:
                pos_montrav = comprehensive_motion_list[self._motors["montrav"]].user_target_pos
            else:
                pos_montrav = get_config().get(self._motors["montrav"]).position
            self._montrav_limit = self._beam_offset * self._montrav_adaptive_value / numpy.tan(numpy.radians(monu_mond_max))
            if pos_montrav > self._montrav_limit:
                error_str = f"\nMotor \"montrav\" out of allowed limits - {pos_montrav:.4f}/{self._montrav_limit:.4f}\n"
                montrav_pos = get_config().get(self._motors["montrav"]).position
                monud_lim = numpy.degrees(numpy.arctan(self._beam_offset * self._montrav_adaptive_value / montrav_pos))
                if self._motors["mond"] in mnames:
                    motor_str = "mond"
                    target_str = target_mond
                else:
                    motor_str = "monu"
                    target_str = target_monu
                error_str += f"Motor \"{motor_str}\" out of allowed limits - {target_str:.4f}/{monud_lim:.4f}"
                self._error = RED(error_str)
                return False
        
        else:
            print(ORANGE("NO CHECK"))

        # Movement is allowed
        return True
        
class SimEncoder:
    def __init__(self, name, config):
        
        self._name = name
        self._config = config
        
        self._motor = config.get("motor")
        
    def read(self):
        return self._motor.position + random.random()/100.
        
        
        
