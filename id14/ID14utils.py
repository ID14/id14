import os
import click
import tabulate
import numpy
import PIL
import contextlib

from bliss import setup_globals, current_session
from bliss.shell.standard import wm, newproposal
from bliss.common.axis import Axis
from bliss.common import measurementgroup, plot
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED, ORANGE
from bliss.controllers.motor import CalcController
from bliss.scanning.scan_tools import get_selected_counter_name
from bliss.config.static import get_config

##################################################################
#####
##### SESSION
#####

##################################################################
#####
##### SOUNDS
#####
def beep():
    print("\a")


##################################################################
#####
##### FLINT
#####
def edit_mask(detector):
    from bliss.common import plot as plot_mdl
    f = plot_mdl.get_flint()
    plot_id = f.get_live_scan_plot(detector.image.fullname, "image")
    plot_mdl.plot_image(existing_id=plot_id)
    p = plot_mdl.plot_image(existing_id=plot_id)
    return p.select_mask()

def curs():
    cnt_name = get_selected_counter_name()
    print("Selected counter: {BLUE(cnt_name}")
    print("\n")
    print("Select a point on flint ...")
    f = plot.get_flint()
    c = f.get_live_plot("default-curve")
    res = c.select_point(1)
    position = c[0]


##################################################################
#####
##### MOTOR POSITIONS PER DEVICES
#####

def whhlm():
    wmcolored(
        get_config().get("monu"),
        get_config().get("tiltu"),
        get_config().get("montrav")
    )
    wmcolored(
        get_config().get("mond"),
        get_config().get("tiltd"),
        get_config().get("monhor")
    )
    wecolored(get_config().get("mond_enc"))

def whpps():
    wmcolored(
        get_config().get("hppst"),
        get_config().get("hppsb"),
        get_config().get("hppsr"),
        get_config().get("hppsh")
    )
    wmcolored(
        get_config().get("hppsvg"),
        get_config().get("hppsvo"),
        get_config().get("hppshg"),
        get_config().get("hppsho")
    )

def ws2():
    wmcolored(
        get_config().get("s2t"),
        get_config().get("s2b"),
        get_config().get("s2r"),
        get_config().get("s2h")
    )
    wmcolored(
        get_config().get("s2vg"),
        get_config().get("s2vo"),
        get_config().get("s2hg"),
        get_config().get("s2ho")
    )
def wundu():
    wmcolored(
        get_config().get("u35b"),
        get_config().get("u35b_taper"),
    )
def wwt():
    wmcolored(
        get_config().get("wtur"),
        get_config().get("wtuh"),
        get_config().get("wtdr"),
        get_config().get("wtdh"),
    )
    wmcolored(
        get_config().get("wtry"),
        get_config().get("wtrz"),
        get_config().get("wty"),
        get_config().get("wtz"),
    )
def wmt():
    wmcolored(
        get_config().get("mtur"),
        get_config().get("mtuh"),
        get_config().get("mtdr"),
        get_config().get("mtdh"),
    )
    wmcolored(
        get_config().get("mtry"),
        get_config().get("mtrz"),
        get_config().get("mty"),
        get_config().get("mtz"),
    )

##################################################################
#####
##### ID14/USERS MACROS
#####
def load_user_script(file_name):
    pass

def load_id14_script(filename):
    current_session.user_script_homedir("/data/id14/inhouse/MACROS")
    current_session.user_script_load(filename, "id14")

#1#################################################################
#####
##### MOTORS
#####
def motor_esync(axis):
    axis.hw_state
    ch = axis.address
    axis.controller.raw_write(f"{ch}:esync")
    axis.controller.raw_write(f"{ch}:power on")
    axis.sync_hard()

def icepap_smaract_enable(axis):
    ch = axis.address
    axis.controller.raw_write(f"{ch}:infoa low normal")
    axis.controller.raw_write(f"{ch}:infoa high normal")

def get_axes_iter():
    for name in dir(setup_globals):
        elem = getattr(setup_globals, name)
        if isinstance(elem, Axis):
            yield elem

def MotorsForceInit():
    for motor in get_axes_iter():
        motor.hw_state

def ApplySessionAxisConfig():
    for motor in get_axes_iter():
        print(f"    \"{motor.name}\"")
        motor.apply_config(reload=True)

def wmcolored(*motors):
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for mot in motors:
        if isinstance(mot.controller, CalcController):
            line1.append(f"{ORANGE(mot.name)}({mot.unit})")
        else:
            line1.append(f"{BLUE(mot.name)}({mot.unit})")
        line2.append(f"{mot.position:.4f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate.tabulate(lines, tablefmt="plain")
    print(mystr)

def wecolored(*encoders):
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for enc in encoders:
        line1.append(f"{BLUE(enc.name)}({enc.unit})")
        line2.append(f"{enc.read():.4f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate.tabulate(lines, tablefmt="plain")
    print(mystr)

