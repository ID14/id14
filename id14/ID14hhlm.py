
import click
import gevent
import time
import numpy

from bliss.common.hook import MotionHook
from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD
from bliss import setup_globals
from bliss.controllers.motor import CalcController
from bliss.config.settings import QueueSetting, ParametersWardrobe
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss import setup_globals, current_session
from bliss.config.static import get_config
from bliss.common.logtools import *

class HHLmono:
    """
    """

    def __init__(self, name, config):
        
        self._config = config
        self._name = name
        
        self._hook = config.get("_par_hook", None)
        if self._hook is None:
            raise RuntimeError("No hook defined")

    def __info__(self):
        print(self._hook._get_info_str())
        return ""
