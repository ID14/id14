/*!----------------------------------------------------------------------------
 *
 * Copyright                 DR. JOHANNES HEIDENHAIN GMBH                  2014
 *
 *  Program    EIB8Driver: Examples
 *
 *  \file      example_poll.c
 *  \author    Bi
 *  \date      23.09.2013
 *  \version   1.0
 *
 *  \brief     Example for connecting the EIB8 and polling measurement data
 *
 *  Content:
 *             Connects to the EIB8 and prints Connection Info
 *             Checks errors and warnings on the EIB8
 *             Configures the EIB8 for measurement
 *             Clears position error
 *             Polls measurment data from the EIB8
 *
 *  History:
 *
 *  23.09.2013: Create document.
 *
 ------------------------------------------------------------------------------*/

/* #####################################
   ###        PLATTFORM SPECIFIC     ###
   ##################################### */

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(_WIN32_WCE)
   #define DEMO_WINDOWS
#else
   #define DEMO_LINUX
#endif

#ifdef DEMO_WINDOWS
   #include <windows.h>

   # define PRId64		"I64d"
#endif

#ifdef DEMO_LINUX
   #include <unistd.h>
   #include <inttypes.h>
   /* Sleep for Linux */
   #define Sleep(x)  usleep(x * 1000)
#endif

/* #####################################
   ###          INCLUDES             ###
   ##################################### */

#include <stdio.h>

/* Include the driver API of the EIB8
   Add driver lib to your linker */
#include "eib8_api.h"

/* Number of PDL packets to be polled: Should be minimal number of packets configurated */
#define EIB8_POLLING_NUM      8u

/* #####################################
   ###           STATICS             ###
   ##################################### */

/* Static error flag */
static int32_t i32GlobalError = 0;

/* Strings used: Avoid stack usage */
static eib8_std_str_t tEibString;
static eib8_std_str_t tStringNode;
static eib8_std_str_t tStringType;
static eib8_std_str_t tStringSource;

/* Configuration for polling position values from EIB8791
   - EIB:  Generate internal PTM (deactivated in configuration, activated after sending configuration to EIB8791)
   - EIB:  Forward PTM to Trigger Bus 1
   - EIB:  Enable Trigger Bus 1 as transmitter
   - SLOT: Forward Trigger Bus 1 to internal SYNC 1 unit
   - SLOT: Enable Trigger Bus 1 and SYNC 1 unit as trigger source
   - SLOT: PDL packet configuration: position:           AXIS01   AXIS02
                                                SLOT01    0x11     0x12
                                                SLOT02    0x21     0x22
                                                SLOT03    0x31     0x32
                                                SLOT04    0x41     0x42
   - AXIS: Set trigger of axis to SYNC 1 unit
   - AXIS: Enable encoder supply
   - AXIS: Start PDL generation

   - Remark:
     Configuration activates 8 axis. If there is no encoder connected, axis generates an error event: "axis input signal amplitue too low"
     For axis not available set (e.g. SLOT04:AXIS00):   SLOT04:AXIS00;trigger:inputs;;
                                                        SLOT04:AXIS00;encoder:supply_enable;0;
                                                        Remove packets not generated from pdl_forwarding_ram_udp
*/


#define NUM_OF_PARAMETER_SETS_POLL_EIB8791     34u
static const eib8_parameter_ptr_string_t tParamListPollEIB8791[NUM_OF_PARAMETER_SETS_POLL_EIB8791] =
  {{ "EIB8",            "trig_ptm_in:source",                     "internal"},            /* Set internal PTM active */
   { "EIB8",            "trig_ptm_in:freq_config",                "0"},                   /* Set internal PTM generator frequency to 0: deactivated */
   { "EIB8",            "trig_bus1_out:inputs",                   "trig_ptm_in"},         /* Forward PTM  to Trigger Bus 1 */
   { "EIB8",            "trig_bus1_out:enable",                   "1"},                   /* Enable Trigger Bus 1 */
   { "EIB8",            "trig_bus1_out:transmitter",              "1"},                   /* Enable Trigger Bus 1 transmitter */
   { "SLOT00",          "trig_sync1_in:input",                    "trig_bus1_in"},        /* All slots: Forward Trigger Bus 1 to internal SYNC 1 unit */
   { "SLOT00",          "trig_bus1_in:enable",                    "1"},                   /* All slots: Enable Trigger Bus 1 */
   { "SLOT00",          "trig_sync1_in:enable",                   "1"},                   /* All slots: Enable SYNC 1 unit */
   { "SLOT01",          "pdl_packets:axis_1",                     "0x11:position"},       /* Slot01: PDL position AXIS01: 0x11 */
   { "SLOT01",          "pdl_packets:axis_2",                     "0x12:position"},       /* Slot01: PDL position AXIS02: 0x12 */
   { "SLOT02",          "pdl_packets:axis_1",                     "0x21:position"},       /* Slot02: PDL position AXIS01: 0x21 */
   { "SLOT02",          "pdl_packets:axis_2",                     "0x22:position"},       /* Slot02: PDL position AXIS02: 0x22 */
   { "SLOT03",          "pdl_packets:axis_1",                     "0x31:position"},       /* Slot03: PDL position AXIS01: 0x31 */
   { "SLOT03",          "pdl_packets:axis_2",                     "0x32:position"},       /* Slot03: PDL position AXIS02: 0x32 */
   { "SLOT04",          "pdl_packets:axis_1",                     "0x41:position"},       /* Slot04: PDL position AXIS01: 0x41 */
   { "SLOT04",          "pdl_packets:axis_2",                     "0x42:position"},       /* Slot04: PDL position AXIS02: 0x42 */
   { "SLOT00:AXIS00",   "trigger:inputs",                         "trig_sync1_in"},       /* All axes: Set trigger of axis to SYNC 1 unit */
   { "SLOT00:AXIS00",   "encoder_config:type",                    "linear"},              /* All axes: Linear encoder */
   { "SLOT00:AXIS00",   "encoder_config:interface",               "1V_pp_0_90"},          /* All axes: Interface 1Vpp */
   { "SLOT00:AXIS00",   "encoder_config:reference_type",          "single"},              /* All axes: Single reference mark */
   { "SLOT00:AXIS00",   "encoder_config:line_cnt",                "0"},                   /* All axes: Line count 0 (only relevant for rotary encoders) */
   { "SLOT00:AXIS00",   "encoder_config:ref_increment",           "0"},                   /* All axes: Not relevant */
   { "SLOT00:AXIS00",   "encoder_config:limit_signal_1_present",  "0"},                   /* All axes: Not relevant */
   { "SLOT00:AXIS00",   "encoder_config:limit_signal_2_present",  "0"},                   /* All axes: Not relevant */
   { "SLOT00:AXIS00",   "encoder_config:homing_signal_present",   "0"},                   /* All axes: Not relevant */
   { "SLOT00:AXIS00",   "encoder_config:limit_signal_2_present",  "0"},                   /* All axes: Not relevant */
   { "SLOT00:AXIS00",   "encoder:supply_enable",                  "1"},                   /* All axes: Enable encoder supply */
   { "SLOT00:AXIS00",   "encoder_processing:online_comp_enable",  "1"},                   /* All axes: Online compensation activate */
   { "SLOT00:AXIS00",   "encoder_processing:waveform_comp_enable","0"},                   /* All axes: Waveform compensation disable */
   { "SLOT00:AXIS00",   "pos_value_filter:active",                "1"},                   /* All axes: Position value filter activate */
   { "SLOT00:AXIS00",   "pos_value_filter:characteristic",        "linear"},              /* All axes: Position value filter characteristic */
   { "SLOT00:AXIS00",   "pos_value_filter:bandwidth",             "high"},                /* All axes: Position value filter bandwidth */
   { "SLOT00:AXIS00",   "pos_value_filter:measure_time",          "0"},                   /* All axes: Position value filter measure time */
   { "SLOT00:AXIS00",   "pdl:output_active",                      "1"}                    /* All axes: Start PDL generation */
   };

/* Configuration as ASCII file (copy section above in txt file) */
#ifdef ASCII_FILE
EIB8;trig_ptm_in:source;internal;
EIB8;trig_ptm_in:freq_config;0;
EIB8;trig_bus1_out:inputs;trig_ptm_in;
EIB8;trig_bus1_out:enable;1;
EIB8;trig_bus1_out:transmitter;1;
SLOT00;trig_sync1_in:input;trig_bus1_in;
SLOT00;trig_bus1_in:enable;1;
SLOT00;trig_sync1_in:enable;1;
SLOT01;pdl_packets:axis_1;0x11:position;
SLOT01;pdl_packets:axis_2;0x12:position;
SLOT02;pdl_packets:axis_1;0x21:position;
SLOT02;pdl_packets:axis_2;0x22:position;
SLOT03;pdl_packets:axis_1;0x31:position;
SLOT03;pdl_packets:axis_2;0x32:position;
SLOT04;pdl_packets:axis_1;0x41:position;
SLOT04;pdl_packets:axis_2;0x42:position;
SLOT00:AXIS00;trigger:inputs;trig_sync1_in;
SLOT00:AXIS00;encoder_config:type;linear;
SLOT00:AXIS00;encoder_config:interface;1V_pp_0_90;
SLOT00:AXIS00;encoder_config:reference_type;single;
SLOT00:AXIS00;encoder_config:line_cnt;0;
SLOT00:AXIS00;encoder_config:ref_increment;0;
SLOT00:AXIS00;encoder_config:limit_signal_1_present;0;
SLOT00:AXIS00;encoder_config:limit_signal_2_present;0;
SLOT00:AXIS00;encoder_config:homing_signal_present;0;
SLOT00:AXIS00;encoder_config:limit_signal_2_present;0;
SLOT00:AXIS00;encoder:supply_enable;1;
SLOT00:AXIS00;encoder_processing:online_comp_enable;1;
SLOT00:AXIS00;encoder_processing:waveform_comp_enable;0;
SLOT00:AXIS00;pos_value_filter:active;1;
SLOT00:AXIS00;pos_value_filter:characteristic;linear;
SLOT00:AXIS00;pos_value_filter:bandwidth;high;
SLOT00:AXIS00;pos_value_filter:measure_time;0;
SLOT00:AXIS00;pdl:output_active;1;
#endif


/* #####################################
   ###         DEFINITIONS           ###
   ##################################### */

/* Print error: Invalid handle allowed */
static void print_error(eib8_err_t tEibError, eib8_handle_t * pEib8Handle)
{
   if (tEibError != EIB8_ERROR_OK)
   {
      eib8_get_last_error(pEib8Handle, &tEibString);
      printf("\n%s\n\n", tEibString);
      i32GlobalError = 1;
   }
}

int main(int argc, char* argv[])
{
   eib8_handle_t tEib8Handle = 0u;
   eib8_connect_check_result_t tEib8Connect = {0u, 0u, 0u, 0u, 0u, 0u, 0u};
   uint32_t u32NumErrors = 0u;
   uint32_t u32NumWarnings = 0u;
   uint32_t u32ErrorWarning = 0u;
   uint32_t u32NumQueueEntries = 0u;
   uint32_t u32Polling = 0u;
   eib8_pdl_packet_t tValuesPoll[EIB8_POLLING_NUM];
   size_t tNumOfPacketsReturned = 0u;
   uint32_t u32Cnt = 0u;
   eib8_pdl_idp8792_pos_status_t tPdlIDP8792PosStatus;

   printf("Starting up EIB8 example poll!\n\n");

   /* Get Version of the driver */
   print_error(eib8_get_driver_version(&tEibString), NULL);
   printf("Version of driver: %s\n", tEibString);

   /* Open the driver */
   print_error(eib8_driver_open(), NULL);

   /* Activate logging to UDP: This computer @ port 3000 (standard port EIB8LogServer.exe) */
   print_error(eib8_activate_logging_udp(EIB8_DRIVER_LOG_LEVEL_WARN, "127.0.0.1", 3000u), NULL);

   /* Get an EIB8 handle */
   print_error(eib8_get_handle(&tEib8Handle), &tEib8Handle);

   /* Connect the EIB8 with IP 192.168.168.2, Port 1050 (factory default of an EIB8 is: 192.168.168.2, Port: 1050) */
   print_error(eib8_connect_tcp(&tEib8Handle, "192.168.168.2", 1050u, &tEib8Connect), &tEib8Handle);
   /* Evaluate returned status of connect */
   printf("     BusyUpdate          %d\n", (int32_t) tEib8Connect.u32BusyUpdate);
   printf("     ErrorHardware       %d\n", (int32_t) tEib8Connect.u32ErrorHardware);
   printf("     ErrorBistDiag       %d\n", (int32_t) tEib8Connect.u32ErrorBistDiag);
   printf("     ErrorUpdate         %d\n", (int32_t) tEib8Connect.u32ErrorUpdate);
   printf("     FactoryMode         %d\n", (int32_t) tEib8Connect.u32FactoryMode);
   printf("     IncompDriver        %d\n", (int32_t) tEib8Connect.u32IncompDriver);
   printf("     PendingEvents       %d\n", (int32_t) tEib8Connect.u32PendingEvents);

   /* Check for events on the EIB8, if connect indicates pending events */
   if (tEib8Connect.u32PendingEvents != 0u)
   {
      /* Read event queues */
      print_error(eib8_check_event_queues(&tEib8Handle, &u32NumErrors, &u32NumWarnings), &tEib8Handle);

      /* Evaluate events */
      if ((u32NumErrors != 0u) || (u32NumWarnings != 0u))
      {
         printf("\n     Error queue:\n");
         do
         {
            print_error(eib8_get_event_queue_entry(&tEib8Handle, &u32NumQueueEntries, NULL, &u32ErrorWarning, &tStringNode, &tStringType, &tStringSource), &tEib8Handle);
            if (u32NumQueueEntries != 0u)
            {
               printf("     %01d  %-10s %-32s %-32s\n", (int32_t) u32ErrorWarning, tStringNode, tStringType, tStringSource);
            }
         } while(u32NumQueueEntries > 1u);
         printf("\n");
      }
   }

   /* Reset EIB8 to idle (stop PDL generation, trigger etc.) */
   print_error(eib8_reset_idle(&tEib8Handle), &tEib8Handle);

   /* Write config */
   print_error(eib8_set_param_list_string(&tEib8Handle, tParamListPollEIB8791, NUM_OF_PARAMETER_SETS_POLL_EIB8791), &tEib8Handle);
   /* Alternatively: Config from file */
   /* print_error(eib8_set_config_file(&tEib8Handle, "filepath", EIB8_FILE_TXT), &tEib8Handle); */

   /* Clear position error */
   print_error(eib8_clear_position_error(&tEib8Handle, "SLOT00:AXIS00"), &tEib8Handle);

   /* Start trigger PTM with 1 kHz */
   print_error(eib8_set_param_string(&tEib8Handle, "EIB8", "trig_ptm_in:freq_config", "1000"), &tEib8Handle);

   /* Poll values from all slots: EIB8_POLLING_NUM is number of packets configurated */
   printf("\n\n     PDL   FRAME    POSITION         POS_ERROR\n");
   for (u32Polling = 0u; u32Polling < 4u; u32Polling++)
   {
      print_error(eib8_poll_pdl_packets(&tEib8Handle, "SLOT00", tValuesPoll, EIB8_POLLING_NUM, &tNumOfPacketsReturned), &tEib8Handle);

      if (i32GlobalError == 0)
      {
         /* print PDL packets */
         printf("\n");
         for (u32Cnt = 0u; u32Cnt < (uint32_t) tNumOfPacketsReturned; u32Cnt++)
         {
            /* Convert to IDP8792 position packet */
            print_error(eib8_pdl_convert_pos_IDP8792(&tValuesPoll[u32Cnt], &tPdlIDP8792PosStatus), &tEib8Handle);
            if (tValuesPoll[u32Cnt].u32ChecksumError != 0u)
            {
               printf("     Erronous packet ...\n");
            }
            else
            {
               printf("     0x%02x  0x%02x     %016"PRId64"     %d\n",
                  tValuesPoll[u32Cnt].u32PacketNumber, tValuesPoll[u32Cnt].u32FrameTransCnt, tPdlIDP8792PosStatus.i64Value48, (int32_t) tPdlIDP8792PosStatus.u32StatusPosError);
               /* Remark: FrameTransmissionCounter is always 0x00 at polling */
            }
         }
      }
   }
   printf("\n");

   /* Reset EIB8 to idle (stop PDL generation, trigger etc.) */
   print_error(eib8_reset_idle(&tEib8Handle), &tEib8Handle);

   /* Close driver (Connection is closed and handle is released automatically) */
   print_error(eib8_driver_close(), NULL);

   return(i32GlobalError);
}
