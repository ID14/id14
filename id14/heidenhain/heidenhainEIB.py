import cffi
import ctypes
import struct

from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.greenlet_utils import protect_from_kill
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController
from bliss.common.protocols import CounterContainer
from bliss.common.logtools import user_warning
import gevent

#
# Mount segfs to modify the server using POGO:
# as root:
#   mount segsrv:/segfs segfs
#
class HeidenhainEIB(CounterContainer):
    def __init__(self, name, config_tree):

        self.name = name

        ip_address = config_tree.get("ip_address")
        port = config_tree.get("socket_port")

        self._eib_ctrl = HeidenhainEIB8791Controller(ip_address, port)
        self._eib_ctrl.clear_position_error()

        # Counters
        self._counters_controller = HeidenhainEIBCounterController(self)
        # Read all channels take 0.135s => reading frequency set to 5Hz
        self._counters_controller.max_sampling_frequency = 10

        counter_node = config_tree.get("counters")
        for config_dict in counter_node:
            if self._counters_controller is not None:
                counter_name = config_dict.get("counter_name")
                HeidenhainEIBCounter(counter_name, config_dict, self._counters_controller)

    def __info__(self):
        info_str = f"\nHeidenhain EIB\nName    : {self.name}\nComm.   : {self._cnx}\n\n"
        return info_str

    @property
    def counters(self):
        return self._counters_controller.counters

    def _read_slot(self, slot):
        #~ breakpoint()
        counter = self._get_counter_from_slot(slot)
        if counter is not None:
            raw_values = self._eib_ctrl.read_one(slot)
            return raw_values[counter.slot] / counter.steps_per_unit
        return None

    def _get_counter_from_slot(self, slot):
        for counter in self.counters:
            if counter.slot == slot:
                return counter
        return None

    def reference_sequence(self, slot):
        self._eib_ctrl.reference_sequence(slot)

    def stop_reference_sequence(self, slot):
        self._eib_ctrl.stop_reference_sequence(slot)

    def configure(self):
        self._eib_ctrl.set_default_configuration()
        self._eib_ctrl.clear_position_error()

    def clear_position_error(self):
        self._eib_ctrl.clear_position_error()

class HeidenhainEIBCounterController(SamplingCounterController):
    def __init__(self, controller):

        self.controller = controller

        super().__init__(self.controller.name, register_counters=False)

        global_map.register(controller, parents_list=["counters"])

    def read_all(self, *counters):
        raw_values = self.controller._eib_ctrl.read_all()
        values = []
        for cnt in counters:
            values.append(raw_values[cnt.slot] / cnt.steps_per_unit)
        return values


class HeidenhainEIBCounter(SamplingCounter):
    def __init__(self, name, config, controller):

        self.slot = config["slot"]
        self.steps_per_unit = config["steps_per_unit"]

        SamplingCounter.__init__(self, name, controller, mode=SamplingMode.MEAN)


class HeidenhainEIB8791Controller:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.open_driver()
        self.connect()

        self.eib8_polling_num = 8

        self.slots = [
            "SLOT01:AXIS01", "SLOT01:AXIS02",
            "SLOT02:AXIS01", "SLOT02:AXIS02",
            "SLOT03:AXIS01", "SLOT03:AXIS02",
            "SLOT04:AXIS01", "SLOT04:AXIS02",
        ]

        self._board_parameters = {
            "trig_ptm_in:source": "internal",
            "trig_ptm_in:freq_config": "1000",
            "trig_bus1_out:inputs": "trig_ptm_in",
            "trig_bus1_out:enable": "1",
            "trig_bus1_out:transmitter": "1",
            "pdl_tx_from_slots_to_lane_1:slot_1": "1",
            "pdl_tx_from_slots_to_lane_1:slot_2": "1",
            "pdl_tx_from_slots_to_lane_1:slot_3": "1",
            "pdl_tx_from_slots_to_lane_1:slot_4": "1",
            "pdl_forwarding_ram_udp:slot_1": "0x11,0x12",
            "pdl_forwarding_ram_udp:slot_2": "0x21,0x22",
            "pdl_forwarding_ram_udp:slot_3": "0x31,0x32",
            "pdl_forwarding_ram_udp:slot_4": "0x41,0x42",
            "udp_transfer:udp_dest_mac": "90.e2.ba.03.9c.eb",
            "udp_transfer:udp_dest_ip": "192.168.168.100",
            "udp_transfer:udp_dest_port": "3051",
            "pdl_forwarding_ram_udp:mode": "batch_soft_realtime",
        }

        self._slot00_parameters = {
            "trig_sync1_in:input": "trig_bus1_in",
            "trig_bus1_in:enable": "1",
            "trig_sync1_in:enable": "1",
        }
        self._slot01_parameters = {
            "pdl_packets:axis_1": "0x11:position",
            "pdl_packets:axis_2": "0x12:position",
        }
        self._slot02_parameters = {
            "pdl_packets:axis_1": "0x21:position",
            "pdl_packets:axis_2": "0x22:position",
        }
        self._slot03_parameters = {
            "pdl_packets:axis_1": "0x31:position",
            "pdl_packets:axis_2": "0x32:position",
        }
        self._slot04_parameters = {
            "pdl_packets:axis_1": "0x41:position",
            "pdl_packets:axis_2": "0x42:position",
        }

        self._axis_parameters = {
            "trigger:inputs": "trig_sync1_in",
            "encoder_config:type": "linear",
            "encoder_config:interface": "1V_pp_0_90",
            "encoder_config:reference_type": "single",
            "encoder_config:line_cnt": "0",
            "encoder_config:ref_increment": "0",
            "encoder_config:limit_signal_1_present": "0",
            "encoder_config:limit_signal_2_present": "0",
            "encoder_config:homing_signal_present": "0",
            "encoder_config:limit_signal_2_present": "0",
            "encoder:supply_enable": "1",
            "encoder_processing:online_comp_enable": "1",
            "encoder_processing:waveform_comp_enable": "0",
            "pos_value_filter:active": "1",
            "pos_value_filter:characteristic": "linear",
            "pos_value_filter:bandwidth": "low",
            "pos_value_filter:measure_time": "0",
            "pdl:output_active": "1",
        }

        self.values = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.positions = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.references = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    def read_one(self, slot):
        eib8_polling_num = 1
        slot_eib = self.ffi.new("const char_t[]", (f"SLOT0".encode() + str(int(slot/2) + 1).encode() + f":AXIS0".encode() + str((slot % 2) + 1).encode() ) )
        self.packets = self.ffi.new("eib8_pdl_packet_t[]", eib8_polling_num)
        leib8_polling_num = self.ffi.new("size_t *", self.eib8_polling_num)
        self.npackets = self.ffi.new("size_t *")
        error = self.lib.eib8_poll_pdl_packets(self.handle, slot_eib, self.packets, eib8_polling_num, self.npackets)
        if error != 0:
            print(self.get_last_error())

        posstatus = self.ffi.new("eib8_pdl_idp8792_pos_status_t *")
        packet = self.ffi.new("eib8_pdl_packet_t*", self.packets[0])
        error = self.lib.eib8_pdl_convert_pos_IDP8792(packet, posstatus)
        if error != 0:
            print(self.get_last_error())
        if posstatus.u32StatusPosError == 0:
            (noval, phase, counter) = struct.unpack("HHi", struct.pack("q", posstatus.i64Value48))
            if self.is_reference_valid(slot):
                reference = self.get_reference_position(slot)
            else:
                reference = 0
            self.values[slot] = counter*65536 - reference*65536 + phase
        else:
            self.values[slot] = 0.0
        return self.values

    def read_all(self):
        slot = self.ffi.new("const char_t[]", f"SLOT00".encode())
        self.packets = self.ffi.new("eib8_pdl_packet_t[]", self.eib8_polling_num)
        eib8_polling_num = self.ffi.new("size_t *", self.eib8_polling_num)
        self.npackets = self.ffi.new("size_t *")
        error = self.lib.eib8_poll_pdl_packets(self.handle, slot, self.packets, self.eib8_polling_num, self.npackets)
        if error != 0:
            print(self.get_last_error())

        posstatus = self.ffi.new("eib8_pdl_idp8792_pos_status_t *")
        for i in range(8):
            packet = self.ffi.new("eib8_pdl_packet_t*", self.packets[i])
            error = self.lib.eib8_pdl_convert_pos_IDP8792(packet, posstatus)
            if error != 0:
                print(self.get_last_error())
            if posstatus.u32StatusPosError == 0:
                (noval, phase, counter) = struct.unpack("HHi", struct.pack("q", posstatus.i64Value48))
                if self.is_reference_valid(i):
                    reference = self.get_reference_position(i)
                else:
                    reference = 0
                self.values[i] = counter - reference + phase
            else:
                self.values[i] = 0.0
        return self.values

    def set_position(self, slot="all"):
        if slot == "all":
            slot_str = "SLOT00:AXIS00"
        else:
            slot_str = self.slots[slot]

        slot_var = self.ffi.new("const char_t[]", slot_str.encode())
        error = self.lib.eib8_set_position_value(self.handle, slot_var, 0)
        if error != 0:
            print(self.get_last_error())

    def reference_sequence(self, slot):
        print("    Start Reference Search ... ", end="")
        self.start_reference_search(slot)
        gevent.sleep(0.1)
        print("Done")
        print("    Waiting Reference Mark Reached .... ", end="\r")
        while not self.is_reference_valid(slot):
            print("    Waiting Reference Mark Reached .... ", end="\r")
            gevent.sleep(0.1)
        print("    Waiting Reference Mark Reached .... Done")
        ref_pos = self.get_reference_position(slot)
        print(f"    Reference Position for slot #{slot} : {ref_pos}")

    def start_reference_search(self, slot):
        slot_var = self.ffi.new("const char_t[]", self.slots[slot].encode())
        error = self.lib.eib8_reference_mark_search(self.handle, slot_var, 1)
        if error != 0:
            print(self.get_last_error())

    def stop_reference_search(self, slot):
        slot_var = self.ffi.new("const char_t[]", self.slots[slot].encode())
        error = self.lib.eib8_reference_mark_search(self.handle, slot_var, 0)
        if error != 0:
            print(self.get_last_error())

    def is_reference_valid(self, slot):
        result = self.get_param_bool(self.slots[slot], "ref_mark:valid") == 1
        return result

    def get_reference_position(self, slot):
        ref_pos = self.get_param_hex32(self.slots[slot], "ref_mark:ref_pos_1")
        (ref, dummy) = struct.unpack("ii", struct.pack("q", ref_pos))
        self.references[slot] = ref
        res = int(self.references[slot])
        return res

    # slot: 0..3    axis: 0..1
    def clear_position_error(self, slot="all"):
        if slot == "all":
            slot_str = "SLOT00:AXIS00"
        else:
            slot_str = self.slots[slot]
        slot_var = self.ffi.new("const char_t[]", slot_str.encode())
        # clear position error
        error = self.lib.eib8_clear_position_error(self.handle, slot_var)
        if error != 0:
            print(self.get_last_error())

    """
    BASIC FUNCTIONS
    """
    def get_last_error(self):
        last_error = self.ffi.new("eib8_std_str_t *")
        error = self.lib.eib8_get_last_error(self.handle, last_error)
        gevent.sleep(0.1)
        if error != 0:
            print(self.ffi.string(last_error[0]).decode())
        return self.ffi.string(last_error[0]).decode()

    def check_events_queue(self, silent=False):
        nerrors = self.ffi.new("uint32_t*")
        nwarnings = self.ffi.new("uint32_t*")
        error = self.lib.eib8_check_event_queues(self.handle, nerrors, nwarnings)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())
        if not silent:
            print(f"    Events Errors: {nerrors[0]}")
            print(f"    Events Warnings: {nwarnings[0]}")
        return (nerrors[0], nwarnings[0])

    def read_events_queue(self):
        (nerrors, nwarnings) = self.check_events_queue(silent=True)
        if nerrors != 0 or nwarnings != 0:
            end = False
            while not end:
                nentries = self.ffi.new("uint32_t*")
                rawevent = self.ffi.new("eib8_event_queue_raw_entry_t*")
                error = self.ffi.new("uint32_t*")
                node = self.ffi.new("eib8_std_str_t*")
                eventtype = self.ffi.new("eib8_std_str_t*")
                eventsource = self.ffi.new("eib8_std_str_t*")
                error = self.lib.eib8_get_event_queue_entry(
                    self.handle,
                    nentries,
                    rawevent,
                    error,
                    node,
                    eventtype,
                    eventsource
                )
                gevent.sleep(0.1)
                if error != 0:
                    print(self.get_last_error())
                str_node = self.ffi.string(node[0]).decode()
                str_eventtype = self.ffi.string(eventtype[0]).decode()
                str_eventsource = self.ffi.string(eventsource[0]).decode()
                #print(error)
                #print(str_node)
                #print(str_eventtype)
                #print(str_eventsource)
                print(f"        {error:01d}  {str_node:10s} {str_eventtype:32s} {str_eventsource:32s}")
                if nentries[0] == 0:
                    end = True
            else:
                print("    Nothing in the event queue")

    # def reset_idle(self):
        # error = self.lib.eib8_reset_idle(self.handle)
        # gevent.sleep(0.1)
        # if error != 0:
            # print(self.get_last_error())

    def get_param_bool(self, node_name, param_name):
        node = self.ffi.new("const char_t[]", node_name.encode())
        param = self.ffi.new("const char_t[]", param_name.encode())
        value = self.ffi.new("uint32_t*")
        error = self.lib.eib8_get_param_bool(self.handle, node, param,  value)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())
        return int(value[0])

    def set_param_bool(self, node_name, param_name, param_value):
        node = self.ffi.new("const char_t[]", node_name.encode())
        param = self.ffi.new("const char_t[]", param_name.encode())
        error = self.lib.eib8_set_param_bool(self.handle, node, param, param_value)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())

    def get_param_hex32(self, node_name, param_name):
        node = self.ffi.new("const char_t[]", node_name.encode())
        param = self.ffi.new("const char_t[]", param_name.encode())
        value = self.ffi.new("uint32_t*")
        error = self.lib.eib8_get_param_hex32(self.handle, node, param,  value)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())
        return value[0]

    def get_param_string(self, node_name, param_name):
        node = self.ffi.new("const char_t[]", node_name.encode())
        param = self.ffi.new("const char_t[]", param_name.encode())
        self.param_value = self.ffi.new("eib8_std_str_t")
        error = self.lib.eib8_get_param_string(self.handle, node, param,  self.param_value)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())
        return self.ffi.string(self.param_value).decode()

    def set_param_string(self, node_name, param_name, param_value):
        node = self.ffi.new("const char_t[]", node_name.encode())
        param = self.ffi.new("const char_t[]", param_name.encode())
        value = self.ffi.new("const char_t[]", param_value.encode())
        error = self.lib.eib8_set_param_string(self.handle, node, param, value)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())

    """
    INITIALISATION
    """
    def _set_default_config(self):
        print("Configuring EIB device ...", end="\r")
        self.set_param_string("EIB8", "trig_ptm_in:source", "internal")
        self.set_param_string("EIB8", "trig_ptm_in:freq_config", "1000")
        self.set_param_string("EIB8", "trig_bus1_out:inputs", "trig_ptm_in")
        self.set_param_string("EIB8", "trig_bus1_out:enable", "1")
        self.set_param_string("EIB8", "trig_bus1_out:transmitter", "1")
        self.set_param_string("EIB8", "pdl_tx_from_slots_to_lane_1:slot_1", "1")
        self.set_param_string("EIB8", "pdl_tx_from_slots_to_lane_1:slot_2", "1")
        self.set_param_string("EIB8", "pdl_tx_from_slots_to_lane_1:slot_3", "1")
        self.set_param_string("EIB8", "pdl_tx_from_slots_to_lane_1:slot_4", "1")
        self.set_param_string("EIB8", "pdl_forwarding_ram_udp:slot_1", "0x11,0x12")
        self.set_param_string("EIB8", "pdl_forwarding_ram_udp:slot_2", "0x21,0x22")
        self.set_param_string("EIB8", "pdl_forwarding_ram_udp:slot_3", "0x31,0x32")
        self.set_param_string("EIB8", "pdl_forwarding_ram_udp:slot_4", "0x41,0x42")
        #
        # Set MAC, IP and port address, where UDP packets of the EIB8 are sent to (example), e.g. discover MAC address with eib8_discover_MAC_from_IP() or EIB8App32/64.exe -> "mac"
        #print("Configuring Network   ...", end="\r")
        #self.set_param_string("EIB8", "udp_transfer:udp_dest_mac", "90.e2.ba.03.9c.eb")
        #self.set_param_string("EIB8", "udp_transfer:udp_dest_ip", "192.168.168.100")
        #self.set_param_string("EIB8", "udp_transfer:udp_dest_port", "3051")
        #self.set_param_string("EIB8", "pdl_forwarding_ram_udp:mode", "batch_soft_realtime")
        #
        print("Configuring common slot parameters   ...", end="\r")
        self.set_param_string("SLOT00", "trig_sync1_in:input", "trig_bus1_in")
        self.set_param_string("SLOT00", "trig_bus1_in:enable", "1")
        self.set_param_string("SLOT00", "trig_sync1_in:enable", "1")
        #
        print("Configuring slot parameters #1   ...", end="\r")
        self.set_param_string("SLOT01", "pdl_packets:axis_1", "0x11:position")
        self.set_param_string("SLOT01", "pdl_packets:axis_2", "0x12:position")
        print("Configuring slot parameters #2   ...", end="\r")
        self.set_param_string("SLOT02", "pdl_packets:axis_1", "0x21:position")
        self.set_param_string("SLOT02", "pdl_packets:axis_2", "0x22:position")
        print("Configuring slot parameters #3   ...", end="\r")
        self.set_param_string("SLOT03", "pdl_packets:axis_1", "0x31:position")
        self.set_param_string("SLOT03", "pdl_packets:axis_2", "0x32:position")
        print("Configuring slot parameters #4   ...", end="\r")
        self.set_param_string("SLOT04", "pdl_packets:axis_1", "0x41:position")
        self.set_param_string("SLOT04", "pdl_packets:axis_2", "0x42:position")
        #
        print("Configuring common parameters  1/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "trigger:inputs", "trig_sync1_in")
        print("Configuring common parameters  2/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:type", "linear")
        print("Configuring common parameters  3/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:interface", "1V_pp_0_90")
        print("Configuring common parameters  4/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:reference_type", "single")
        print("Configuring common parameters  5/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:line_cnt", "0")
        print("Configuring common parameters  6/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:ref_increment", "0")
        print("Configuring common parameters  7/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:limit_signal_1_present", "0")
        print("Configuring common parameters  8/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:limit_signal_2_present", "0")
        print("Configuring common parameters  9/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_config:homing_signal_present", "0")
        print("Configuring common parameters 10/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder:supply_enable", "1")
        print("Configuring common parameters 11/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_processing:online_comp_enable", "1")
        print("Configuring common parameters 12/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "encoder_processing:waveform_comp_enable", "0")
        print("Configuring common parameters 13/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "pos_value_filter:active", "1")
        print("Configuring common parameters 14/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "pos_value_filter:characteristic", "linear")
        print("Configuring common parameters 15/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "pos_value_filter:bandwidth", "low")
        print("Configuring common parameters 16/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "pos_value_filter:measure_time", "0")
        print("Configuring common parameters  17/17", end="\r")
        self.set_param_string("SLOT00:AXIS00", "pdl:output_active", "1")

    def set_default_config(self):
        self._config_board()
        for i in range(8):
            self._config_axis(i)

    def _config_board(self):
        print("Configure EIB8 Board")
        for param,value in self._board_parameters.items():
            print(f"    EIB8 -  {param} - {value}                      ", end="\r")
            self.set_param_string("EIB8", param, value)

        print("\nConfigure Common parameters")
        for param,value in self._slot00_parameters.items():
            print(f"    SLOT00 -  {param} - {value}                      ", end="\r")
            self.set_param_string("SLOT00", param, value)

        print("\nConfigure SLOT01 parameters")
        for param,value in self._slot01_parameters.items():
            print(f"    SLOT01 -  {param} - {value}                      ", end="\r")
            self.set_param_string("SLOT01", param, value)

        print("\nConfigure SLOT02 parameters")
        for param,value in self._slot02_parameters.items():
            print(f"    SLOT02 -  {param} - {value}                      ", end="\r")
            self.set_param_string("SLOT02", param, value)

        print("\nConfigure SLOT03 parameters")
        for param,value in self._slot03_parameters.items():
            print(f"    SLOT03 -  {param} - {value}                      ", end="\r")
            self.set_param_string("SLOT03", param, value)

        print("\nConfigure SLOT04 parameters")
        for param,value in self._slot04_parameters.items():
            print(f"    SLOT04 -  {param} - {value}                      ", end="\r")
            self.set_param_string("SLOT04", param, value)

    def _config_axis(self, slot):
        axis = self.slots[slot]
        print(f"\nConfigure axis {axis}")
        for param,value in self._axis_parameters.items():
            print(f"    {slot} -  {param} - {value}                      ", end="\r")
            self.set_param_string(axis, param, value)

    def set_default_configuration(self):
        error = self.lib.eib8_set_default_conf_EIB8791(self.handle, "".encode(), "".encode(), 0)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())

    def connect(self):
        check_result = self.ffi.new("eib8_connect_check_result_t *")
        hostname = self.ffi.new("const char_t[]", self.ip.encode())
        error = self.lib.eib8_connect_tcp(self.handle, hostname, self.port, check_result)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())

    def open_driver(self):

        self.ffi = cffi.FFI()
        self.ffi.cdef(
            """
typedef char char_t;

typedef uint32_t char32_t;

typedef struct
{
   uint32_t u32BusyUpdate;
   uint32_t u32ErrorUpdate;
   uint32_t u32FactoryMode;
   uint32_t u32ErrorHardware;
   uint32_t u32ErrorBistDiag;
   uint32_t u32PendingEvents;
   uint32_t u32IncompDriver;
} eib8_connect_check_result_t;

typedef enum
{
   EIB8_ERROR_OK                       = 0,
   EIB8_ERROR_GENERAL                  = 5001,
   EIB8_ERROR_DRIVER_INTERNAL          = 5002,
   EIB8_ERROR_DRIVER_INPUT_DATA        = 5003,
   EIB8_ERROR_INVALID_RESOURCE         = 5004,
   EIB8_ERROR_DRIVER_NOT_OPENED        = 5005,
   EIB8_ERROR_DRIVER_ALREADY_OPENED    = 5006,
   EIB8_ERROR_HANDLE_NOT_CONNECTED     = 5007,
   EIB8_ERROR_EIB8_CONNECT_FAIL        = 5008,
   EIB8_HIGH_LEVEL_OBJECT_NULL         = 5009,

   EIB8_ERROR_EIB8_BUSY_UPDATE         = 5014,
   EIB8_ERROR_EIB8_CMD_BUSY            = 5015,
   EIB8_ERROR_EIB8_CMD_NOT_SUPPORTED   = 5016,
   EIB8_ERROR_EIB8_CMD_TIMEOUT         = 5017,
   EIB8_ERROR_EIB8_CMD_FAILED          = 5018,

   EIB8_ERROR_PARAM_NODE_INVALID       = 5019,
   EIB8_ERROR_PARAM_NAME_INVALID       = 5020,
   EIB8_ERROR_PARAM_VALUE_INVALID      = 5021,
   EIB8_ERROR_PARAM_IS_READONLY        = 5022,

   EIB8_ERROR_SAMPLING_QUEUE_OVERRUN   = 5023,
   EIB8_ERROR_SAMPLING_INVALID_DATA    = 5024,
   EIB8_ERROR_SAMPLING_NOT_ACTIVE      = 5025,
   EIB8_ERROR_SAMPLING_FRAME_CNT_ERR   = 5026,
   EIB8_ERROR_SAMPLING_TIMEOUT         = 5027
} eib8_err_t;
typedef uint32_t eib8_handle_t;
typedef char_t eib8_std_str_t[256u];
typedef struct
{
   uint64_t    u64Value64;                      /*!< 64 Bit payload of pdl packet */
   uint32_t    u32PacketNumber;                 /*!< Packet number of pdl packet: 0x00 - 0xFF */
   uint32_t    u32FrameTransCnt;                /*!< Frame transmission count of pdl packet: 0x00 - 0xFF */
   uint32_t    u32ChecksumError;                /*!< Flag that there is a checksum error within the PDL packet: 0: checksum OK, 1: checksum fail */
   uint32_t    u32Dummy;                        /*!< Due to 64 bit alignment on various platforms */
} eib8_pdl_packet_t;
typedef struct
{
   int64_t     i64Value48;                      /*!< 48 Bit signed value of payload as position, velocity or acceleration (last significant 16 Bits always 0: 0xXXXX XXXX XXXX 0000) */
   uint32_t    u32StatusPosError;               /*!< Position error flag: 0: position OK, 1: position invalid */
   uint32_t    u32StatusEntryErrorQueue;        /*!< Flag that there is an entry in the error queue: 0: no entry, 1: entry */
   uint32_t    u32LimitL2SignalActive;          /*!< 1 = Limit L2 input signal active, 0 = inactive */
   uint32_t    u32HomingL1SignalActive;         /*!< 1 = Homing L1 input signal active, 0 = inactive */
   uint32_t    u32RefMarkValid;                 /*!< 1 = Reference mark is valid, reference position can be used */
} eib8_pdl_idp8792_pos_status_t;
typedef struct
{
   uint32_t       u32CommandGroup;              /*!< Command goup of the event */
   uint32_t       u32Error;                     /*!< Indicates, if error or warning (0: warning, 1: error) */
   uint32_t       u32EventType;                 /*!< Event type: Value 0x00..0x7F: warning, 0x80 .. 0xFF: error */
   uint32_t       u32EventSourceMask;           /*!< Indicates, from which source the event comes from (lower 16 bit are valid, value is a bitmask) */
   uint32_t       u32UtcTimestamp;              /*!< Timestamp (UTC) at which event occured */
   uint32_t       u32Parameter1;                /*!< Additional parameter 1 */
   uint32_t       u32Parameter2;                /*!< Additional parameter 2 */
} eib8_event_queue_raw_entry_t;
typedef struct
{
   eib8_std_str_t tNode;                        /*!< Node,      e.g. "SLOT01" */
   eib8_std_str_t tParam;                       /*!< Parameter, e.g. "network_settings:ip" */
   eib8_std_str_t tValue;                       /*!< Value,     e.g. "192.168.168.2" */
} eib8_parameter_string_t;
typedef enum
{
   EIB8_DISABLE      = 0,                       /*!< Disable */
   EIB8_ENABLE       = 1                        /*!< Enable */
} eib8_enable_t;

extern eib8_err_t eib8_get_driver_version(eib8_std_str_t* pStrDriverVersion);
extern eib8_err_t eib8_driver_open();
extern eib8_err_t eib8_driver_close();
extern eib8_err_t eib8_get_handle(eib8_handle_t* pEib8Handle);
extern eib8_err_t eib8_connect_tcp(eib8_handle_t* pEib8Handle, const char_t* pStrIp, uint32_t u32Port, eib8_connect_check_result_t* pCheckResult);
extern void eib8_get_last_error(eib8_handle_t* pEib8Handle, eib8_std_str_t* pStrError);
extern eib8_err_t eib8_reset_idle(eib8_handle_t* pEib8Handle);
extern eib8_err_t eib8_clear_position_error(eib8_handle_t* pEib8Handle, const char_t* pNode);
extern eib8_err_t eib8_poll_pdl_packets(eib8_handle_t* pEib8Handle, const char_t* pNode, eib8_pdl_packet_t pValuesPoll[], size_t tMaxNumOfValues, size_t* pNumOfReturnedValues);
extern eib8_err_t eib8_pdl_convert_pos_IDP8792(const eib8_pdl_packet_t* pPdlData, eib8_pdl_idp8792_pos_status_t * pPdlIDP8792PosStatus);
extern eib8_err_t eib8_check_event_queues(eib8_handle_t* pEib8Handle, uint32_t* pNumOfErrors, uint32_t* pNumOfWarnings);
extern eib8_err_t eib8_get_event_queue_entry(eib8_handle_t* pEib8Handle, uint32_t*pNumOfEntries, eib8_event_queue_raw_entry_t* pRawEvent, uint32_t* pError, eib8_std_str_t * pNode, eib8_std_str_t * pEventType, eib8_std_str_t * pEventSource);
extern eib8_err_t eib8_get_config_all_list(eib8_handle_t* pEib8Handle, eib8_parameter_string_t pParamList[], size_t tNumOfElements, size_t* pNumOfParamOut, size_t* pNumOfParamAvailable);
extern eib8_err_t eib8_get_param_bool(eib8_handle_t* pEib8Handle, const char_t* pNode, const char_t* pParam,  uint32_t* pValue);
extern eib8_err_t eib8_set_param_bool(eib8_handle_t* pEib8Handle, const char_t* pNode, const char_t* pParam, uint32_t u32Value);
extern eib8_err_t eib8_get_param_string(eib8_handle_t* pEib8Handle, const char_t* pNode, const char_t* pParam,  eib8_std_str_t* pValue);
extern eib8_err_t eib8_set_param_string(eib8_handle_t* pEib8Handle, const char_t* pNode, const char_t* pParam, const char_t* pValue);
extern eib8_err_t eib8_set_position_value(eib8_handle_t* pEib8Handle, const char_t* pNode, int64_t i64Position48);
extern eib8_err_t eib8_reference_mark_search(eib8_handle_t* pEib8Handle, const char_t* pNode, eib8_enable_t tEnable);
extern eib8_err_t eib8_get_param_hex32(eib8_handle_t* pEib8Handle, const char_t* pNode, const char_t* pParam,  uint32_t* pValue);
extern eib8_err_t eib8_set_default_conf_EIB8791(eib8_handle_t* pEib8Handle, const char_t* pStrUdpMAC, const char_t* pStrUdpIP, uint32_t u32UdpPort);
"""
)
        low_level_library = "/users/blissadm/local/id14.git/id14/heidenhain/lib/libEIB8Driver.so"
        self.lib = self.ffi.dlopen(low_level_library)

        version = self.ffi.new("eib8_std_str_t*")
        error = self.lib.eib8_get_driver_version(version)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())
        version_str = self.ffi.string(version[0]).decode()
        print(f"Heidenhain EIB: {version_str}")

        error = self.lib.eib8_driver_open()
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())

        self.handle = self.ffi.new("eib8_handle_t *")
        error = self.lib.eib8_get_handle(self.handle)
        gevent.sleep(0.1)
        if error != 0:
            print(self.get_last_error())

