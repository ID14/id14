from bliss.controllers.motor import Controller
from bliss.comm.util import get_comm
from bliss.common.axis import AxisState

"""
Bliss controller for the ID14 kohzu monochromator
"""

# ~ low_monu        =    0.8884 # lower limit for motor monu
# ~ high_monu       =   20.9884 # upper limit for motor monu
# ~ low_mond        =    1.1355 # lower limit for motor mond
# ~ high_mond       =   21.1355 # upper limit for motor mond
# ~ low_montrav     =    5.0    # lower limit for motor montrav
# ~ high_montrav    =  400.50   # upper limit for motor montrav


############### Angle to energy #################


# ~ ID18MONO["offset"]     = 20.39    # Offset of the reflected beam to the incoming beam [mm]


# ~ ID18MONO["calcns"]     =  1.0004  # correction for mond stage
# ~ ID18MONO["vert_angle"] = 11.6605  # vertical position of sinur bar for mond
# ~ ID18MONO["conversion"] = 12.39854  # energy-wavelength conversion constant


# ~ bragg_angle     = ID18MONO["vert_angle"] + deg(asin(rad(pos_mond/ID18MONO["calcns"] - ID18MONO["vert_angle"])))
#                                               using  mond ^^^^^^^^
# ~ ID18lambda          = 2 * ID18MONO["a111"] * sin(rad(bragg_angle))
# ~ current_energy  = ID18MONO["conversion"] / ID18lambda
  

############### Energy to angle #################


# ~ ID18lambda          = ID18MONO["conversion"] / requested_energy
# ~ bragg_angle     = asin(ID18lambda / (2 * ID18MONO["a111"]))

# ~ # those are the new positions of the motors, they must be stored in some
# ~ # globally accessible place, dial angles here
# ~ pos_montrav       = ID18MONO["offset"] / tan(2 * bragg_angle)
# ~ pos_mond          = ID18MONO["vert_angle"] + deg(sin(bragg_angle - rad(ID18MONO["vert_angle"])))
# ~ pos_mond          = pos_mond * ID18MONO["calcns"]
# ~ pos_monu          = pos_mond

#### ST_DEST contains the destination angles for our three motors.
# ~ # choose the right sequence of motor movements
# ~ if (ST_DEST["montrav"] >= pos_montrav) {
# ~ print "Moving monu to", ST_DEST["monu"], "and mond to", ST_DEST["mond"]
# ~ if (ST_move_angles(ST_DEST["monu"], ST_DEST["mond"])) {
  # ~ print
  # ~ print "Command \"$0 $*\" aborted."
  # ~ exit
# ~ }
# ~ print
# ~ print "Moving montrav to ", user(montrav, ST_DEST["montrav"])
    # ~ A[montrav] = user(montrav, ST_DEST["montrav"])
# ~ move_em; _update("montrav")
# ~ } else {
# ~ print "Moving montrav to ", user(montrav, ST_DEST["montrav"])
    # ~ A[montrav] = user(montrav, ST_DEST["montrav"])
# ~ move_em; _update("montrav")
# ~ print "Moving monu to", ST_DEST["monu"], "and mond to", ST_DEST["mond"]
# ~ if (ST_move_angles(ST_DEST["monu"], ST_DEST["mond"])) {
  # ~ print
  # ~ print "Command \"$0 $*\" aborted."
  # ~ exit
# ~ }
# ~ }
