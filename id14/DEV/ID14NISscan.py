# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
ID14 NIS scan

Ease the users' life
"""

import time
import math
import gevent
import tabulate
import sys
from typing import Optional


# ~ from bliss import global_map
from bliss.common.logtools import log_error, user_print

from bliss.shell.cli.user_dialog import (
    UserMsg,
    UserYesNo,
    UserChoice,
    UserFloatInput,
    Container,
    UserInput,
    UserFileInput,
)

from bliss.shell.cli.pt_widgets import display, BlissDialog
from bliss import current_session, is_bliss_shell, global_map
from bliss.config import settings
from bliss.common.scans import dnscan
from bliss.config.static import get_config
from bliss.common.types import (
    _int,
    _float,
    _countable,
    _countables,
    _scannable_start_stop_list,
    _position_list,
    _scannable_position_list_list,
)
from bliss.shell.getval import *
from bliss.shell.getval import _prompt_factory, _BlissKeyboardInterrupt
from prompt_toolkit.completion import WordCompleter
from prompt_toolkit.formatted_text import to_formatted_text, HTML
from prompt_toolkit import print_formatted_text
from prompt_toolkit import prompt
from prompt_toolkit.validation import Validator, ValidationError


__author__ = "Holger Witsch - ESRF ISDD SOFTGROUP BLISS - February 2023"


def NISscansetup():
    """
    NISscan Setup Menu

    Access to the BlissDialog-based menu from shell.
    set up the internal variables used by the NISscan function. Needed to be run the very first time. Opens a menu which allowing
    to set parameters interactively.
    """

    # get possible former setup from redis hash
    NISscansettings = settings.OrderedHashSetting("NISscan")

    nis_list = []
    for x in NISscansettings.keys():
        nis_list.append([x, NISscansettings[x]])

    # ~ user_print(nis_list) # only for author

    def get_axes_names_iter():
        for axis in global_map.get_axes_iter():
            if axis._positioner:
                yield axis.name

    class trialContextManager:
        def __enter__(self):
            pass

        def __exit__(self, *args):
            return True

    trial = trialContextManager()

    # ~ class FloatValidator(Validator):
        # ~ def validate(self, document):
            # ~ text = document.text
            # ~ if text and not float(text):
                # ~ raise ValidationError(
                    # ~ message="This input contains non-float characters"
                # ~ )


    def ID14_prompt(
        message,
        default=None,
        validator=None,
        completer=None,
        complete_while_typing=True,
    ):
        """
        Default ptpython prompt embedded inside a thread to make it work with BLISS.
        """

        session = _prompt_factory()
        try:
            return asyncio_gevent.yield_future(
                session.prompt_async(
                    message,
                    validator=validator,
                    handle_sigint=False,
                    style=get_style(),
                    completer=completer,
                    complete_while_typing=complete_while_typing,
                    default=default,
                )
            )
        except _BlissKeyboardInterrupt:
            raise KeyboardInterrupt

    motor_names = list(get_axes_names_iter())
    html = HTML("          <bbb>NISscansetup</bbb>!")
    text = to_formatted_text(html, style="class:my_html fg:#0000bb")
    print_formatted_text(text)
    subtitle("Leave motorname empty, if not needed.".center(50))
    print_formatted_text()

    motor_completer = WordCompleter(motor_names)
    ordnums = ["first", "second", "third", "fourth"]
    for _ in range(4):
        prstr = "Select the " + ordnums[_] + " motor: "
        nis_list[_][0] = ID14_prompt(prstr, completer=motor_completer, complete_while_typing=True, default=nis_list[_][0])
        # I never got the validator to work.  Not even on another system...
        nis_list[_][1] = float(ID14_prompt('with the constant     : ', default=str(nis_list[_][1]))) #, validator=FloatValidator))

    # ~ user_print(nis_list)  # only for author

    NISscansettings.clear()
    if nis_list[0][0] : NISscansettings[nis_list[0][0]] = nis_list[0][1]
    if nis_list[1][0] : NISscansettings[nis_list[1][0]] = nis_list[1][1]
    if nis_list[2][0] : NISscansettings[nis_list[2][0]] = nis_list[2][1]
    if nis_list[3][0] : NISscansettings[nis_list[3][0]] = nis_list[3][1]


def NISscanscreen():
    """
    NISscan Setup Menu

    Access to the BlissDialog-based menu from shell.
    set up the internal variables used by the NISscan function. Needed to be run the very first time. Opens a menu which allowing
    to set parameters interactively.
    """

    # get possible former setup from redis hash
    NISscansettings = settings.OrderedHashSetting("NISscan")

    nis_list = []
    for x in NISscansettings.keys():
        nis_list.append([x, NISscansettings[x]])

    # ~ user_print(nis_list) # only for author

    def get_axes_names_iter():
        for axis in global_map.get_axes_iter():
            if axis._positioner:
                yield axis.name

    class trialContextManager:
        def __enter__(self):
            pass

        def __exit__(self, *args):
            return True

    trial = trialContextManager()

    motor_names = list(get_axes_names_iter())
    dlgmsg = UserMsg(label="Leave motorname empty, if not needed")

    dlg1m = UserInput(label="Select the first motor", completer=motor_names)
    with trial:
        dlg1m.defval = nis_list[0][0]
    dlg1c = UserFloatInput(label="with the costant")
    with trial:
        dlg1c.defval = nis_list[0][1]

    dlg2m = UserInput(label="Select the second motor", completer=motor_names)
    with trial:
        dlg2m.defval = nis_list[1][0]
    dlg2c = UserFloatInput(label="with the costant")
    with trial:
        dlg2c.defval = nis_list[1][1]

    dlg3m = UserInput(label="Select the third motor", completer=motor_names)
    with trial:
        dlg3m.defval = nis_list[2][0]
    dlg3c = UserFloatInput(label="with the costant")
    with trial:
        dlg3c.defval = nis_list[2][1]

    dlg4m = UserInput(label="Select the fourth motor", completer=motor_names)
    with trial:
        dlg4m.defval = nis_list[3][0]
    dlg4c = UserFloatInput(label="with the costant")
    with trial:
        dlg4c.defval = nis_list[3][1]

    ct1 = Container([dlg1m, dlg1c], title="Motor 1")
    ct2 = Container([dlg2m, dlg2c], title="Motor 2")
    ct3 = Container([dlg3m, dlg3c], title="Motor 3")
    ct4 = Container([dlg4m, dlg4c], title="Motor 4")

    try:
        BlissDialog([[dlgmsg], [ct1], [ct2], [ct3], [ct4]], title="NISscan").show()
    except:
        pass
    else:
        NISscansettings.clear()
        if dlg1m.defval:
            NISscansettings[dlg1m.defval] = dlg1c.defval
        if dlg2m.defval:
            NISscansettings[dlg2m.defval] = dlg2c.defval
        if dlg3m.defval:
            NISscansettings[dlg3m.defval] = dlg3c.defval
        if dlg4m.defval:
            NISscansettings[dlg4m.defval] = dlg4c.defval


def NISscan(
    Einit: _float,
    Efinal: _float,
    intervals: _int,
    count_time: _float,
    *counter_args: _countables,
    save: bool = True,
    save_images: Optional[bool] = None,
    sleep_time: Optional[_float] = None,
    stab_time: Optional[_float] = None,
    run: bool = True,
    return_scan: bool = True,
    scan_info: Optional[dict] = None,
):
    """
    NIS Scan

    Construct the scan and run it.
    """

    # get possible former setup from redis hash
    NISscansettings = settings.OrderedHashSetting("NISscan")

    nis_list = []
    for x in NISscansettings.keys():
        nis_list.append([x, NISscansettings[x]])

    # ~ user_print(nis_list) # only for author

    mnum = len(nis_list)
    if not mnum:
        return

    motor_tuple_list = []
    for _ in range(mnum):
        mne = get_config().get(nis_list[_][0])
        const = nis_list[_][1]
        motor_tuple_list.append((mne, Einit * const, Efinal * const))

    user_print(motor_tuple_list)

    scan = dnscan(
        motor_tuple_list,
        intervals,
        count_time,
        counter_args,
        save=save,
        save_images=save_images,
        title="NISscan",
        name="NISscan",
        scan_type="dscan",
        sleep_time=sleep_time,
        stab_time=stab_time,
        run=False,
        scan_info=scan_info,
    )
    if run:
        scan.run()

    if return_scan:
        return scan
