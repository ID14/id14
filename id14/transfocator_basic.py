
import os
import glob
import numpy
import gevent

from bliss.controllers.bliss_controller import BlissController
from bliss.config import settings

class Transfocator(BlissController):

    def __init__(self, config):
        super().__init__(config)

    def _load_config(self):
        super()._load_config()

        self._wago = self.config.get("wago", None)
        if self._wago is None:
            raise RuntimeError("Transfocator: No Wago Object Configured")
        self._prefix = self.config.get("prefix", None)
        if self._prefix is None:
            raise RuntimeError("SpeedgoatScope: No prefix Configured")

        self._configure()

    def __info__(self):
        mystr = ""
        mystr += f"Pinhole\n"
        for i in range(self._pin_nb):
            mystr += f"    #{i+1}: {self._pinhole_state(i+1)}\n"
        mystr += f"Lenses\n"
        for i in range(self._lens_nb):
            mystr += f"    #{i+1}: {self._lens_state(i+1)}\n"
        return mystr

    def _configure(self):
        # names
        self._pin_in_name = f"{self._prefix}_pinhole_in"
        self._pin_out_name = f"{self._prefix}_pinhole_out"
        self._pin_ctrl_name = f"{self._prefix}_pinhole_ctrl"
        self._lens_in_name = f"{self._prefix}_lens_in"
        self._lens_out_name = f"{self._prefix}_lens_out"
        self._lens_ctrl_name = f"{self._prefix}_lens_ctrl"

        # pinhole
        if self._pin_in_name in self._wago.logical_keys:
            pin = self._wago.get(self._pin_in_name)
            if pin is None:
                nb_in = 0
                nb_out = 0
                nb_ctrl = 0
            elif isinstance(pin, list):
                nb_in = len(self._wago.get(self._pin_in_name))
                nb_out = len(self._wago.get(self._pin_out_name))
                nb_ctrl = len(self._wago.get(self._pin_ctrl_name))
            else:
                nb_in = 1
                nb_out = 1
                nb_ctrl = 1
            if nb_in == nb_out == nb_ctrl:
                self._pin_nb = nb_in
            else:
                raise RuntimeError(f"Transfocator: Wrong pinhole configuration (in {nb_in} - out {nb_out} - ctrl {nb_ctrl})")

        # lens
        lens = self._wago.get(self._lens_in_name)
        if lens is None:
            nb_in = 0
            nb_out = 0
            nb_ctrl = 0
        elif isinstance(lens, list):
            nb_in = len(self._wago.get(self._lens_in_name))
            nb_out = len(self._wago.get(self._lens_out_name))
            nb_ctrl = len(self._wago.get(self._lens_ctrl_name))
        else:
            nb_in = 1
            nb_out = 1
            nb_ctrl = 1
        if nb_in == nb_out == nb_ctrl:
            self._lens_nb = nb_in
        else:
            raise RuntimeError(f"Transfocator: Wrong lens configuration (in {nb_in} - out {nb_out} - ctrl {nb_ctrl}")
        if self._lens_nb == 0:
            raise RuntimeError(f"Transfocator: No lens configured")

    def _lens_state(self, num):
        if num >= 1 and num <= self._lens_nb:
            in_state = self._wago.get(self._lens_in_name)
            out_state = self._wago.get(self._lens_out_name)
            if self._lens_nb == 1:
                return self._get_state(in_state, out_state)
            else:
                return self._get_state(in_state[num-1], out_state[num-1])
        else:
            return "No Pinhole Configured"

    def _pinhole_state(self, num):
        if num >= 1 and num <= self._pin_nb:
            in_state = self._wago.get(self._pin_in_name)
            out_state = self._wago.get(self._pin_out_name)
            if self._pin_nb == 1:
                return self._get_state(in_state, out_state)
            else:
                return self._get_state(in_state[num-1], out_state[num-1])
        else:
            return "No Pinhole Configured"

    def _get_state(self, in_state, out_state):
        if in_state == 1:
            if out_state == 0:
                return "IN"
            else:
                return "ERROR"
        else:
            if out_state == 0:
                return "MOVING"
            else:
                return "OUT"

    def set_lens_in(self, *lens):
        self._set_lens(lens, 1)

    def set_lens_out(self, *lens):
        self._set_lens(lens, 0)

    def _set_lens(self, lens, state):
        if isinstance(lens, list) or isinstance(lens, tuple):
            lens2set = lens
        else:
            lens2set = [lens]
        if state == 1:
            if self._pin_nb > 1:
                self.set_pinhole_in(1)
                while self._pinhole_state(1) != "IN":
                    gevent.sleep(0.1)
        self._set_channels(self._lens_ctrl_name, self._lens_nb, lens2set, state)

    def set_pinhole_in(self, pinhole=None):
        self._set_pinhole(pinhole, 1)

    def set_pinhole_out(self, pinhole=None):
        self._set_pinhole(pinhole, 0)

    def _set_pinhole(self, pinhole, state):
        if pinhole is None:
            pinhole2set = 1
        else:
            pinhole2set = pinhole
        if isinstance(pinhole, list) or isinstance(pinhole, tuple):
            pinhole2set = pinhole
        else:
            pinhole2set = [pinhole]
        self._set_channels(self._pin_ctrl_name, self._pin_nb, pinhole2set, state)

    def _set_channels(self, name, max_nb, channels, state):
        if max_nb == 1:
            self._wago.set(name, state)
        else:
            channels_state = self._wago.get(name)
            for i in range(max_nb):
                if (i+1) in channels:
                    channels_state[i] = int(state)
            self._wago.set(name, channels_state)


