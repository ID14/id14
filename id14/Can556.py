
import gevent
import numpy
import struct

from bliss.shell.standard import flint
from bliss.common.tango import DeviceProxy, DevState, DevFailed
from bliss.common.utils import autocomplete_property
from bliss.common.cleanup import cleanup
from bliss.controllers.bliss_controller import BlissController
from bliss.scanning.group import Sequence
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.chain import AcquisitionChannel

from bliss.config.settings import SimpleSetting
from bliss.config.settings import HashSetting

from id14.ID24menu import menu_list, menu_number, menu_word

class Can556HardwareController:

    def __init__(self, name):
        self._name = name

    def init_communication(self, tango_uri):
        self._tango_uri = tango_uri
        self.proxy = DeviceProxy(self._tango_uri)

    def get_data(self):
        return self.proxy.data

    def start(self):
        self.proxy.on()

    def stop(self):
        self.proxy.off()

    @property
    def acq_time(self):
        return self.proxy.PresetLiveTime

    @acq_time.setter
    def acq_time(self, value):
        self.proxy.PresetLiveTime = value

    def state(self):
        return self.proxy.state()

    def status(self):
        return self.proxy.status()

    @property
    def channels(self):
        return self.proxy.Channels

    @channels.setter
    def channels(self, value):
        self.proxy.Channels = value

class Can556(BlissController):

    def __init__(self, config):

        BlissController.__init__(self, config)

        self._hwc = Can556HardwareController(self.name)
        #self._icc = MyDetIntegratingCounterController(self.name, self)

        #self._icc.create_counter(MyDetCounter, name="image")

        #global_map.register(self, parents_list=["counters"])

        self._data = None

        self.calibration = Can556Calibration(self)

        self.rois = Can556RoiContainer(self)

    def _load_config(self):
        """
        Read and apply the YML configuration of this container.=
        """

        # Get communication config parameters and apply it to the HardwareController
        tango_uri = self.config.get("uri", str)
        self._hwc.init_communication(tango_uri)

    def _init(self):
        # Called by the plugin via self._initialize_config
        # Called just after self._load_config

        """
        Place holder for any action to perform after the configuration has been loaded.
        """
        pass

    def _get_default_chain_counter_controller(self):
        """
        Return the default counter controller that should be used
        when this controller is used to customize the DEFAULT_CHAIN
        """
        pass
        #return self._icc

    @autocomplete_property
    def counters(self):
        pass
        #return self._icc.counters

    def __info__(self):
        """Return controller info as a string"""
        # ~ txt = self.get_info()
        txt  = (f"Name: {self.name}\n"
                f"Device server name: {self._hwc.proxy.dev_name()}\n"
                f"Mode:               {self._hwc.proxy.Mode}\n"
                f"Channels:           {self.channels}\n"
               )
        return txt

    @property
    def channels(self):
        return self._hwc.channels

    @channels.setter
    def channels(self, value):
        self._hwc.Channels = value

    def _stop_acq(self):
        self._hwc.stop()

    def save(self):
        title = f"{self.name}.acq {self._acq_time}"
        instrument = {
            calibration: {
                "a": self.calibration.a,
                "b": self.calibration.b,
            }
        }

        scan_info = ScanInfo()
        scan_info.update(
            {
                "title": title,
                "type": "MCA Acq",
                "instrument": instrument,
                "dim": 1,
            }
        )
        self._seq = Sequence(scan_info=scan_info, title=title)
        self._seq.add_custom_channel(AcquisitionChannel("Counts",numpy.float64,()))
        self._seq.add_custom_channel(AcquisitionChannel("Channels",numpy.float64,()))
        self._seq.add_custom_channel(AcquisitionChannel("Time",numpy.float64,()))

        with self._seq.sequence_context() as scan_seq:
            self._seq.custom_channels["Channels"].emit(self._datax)
            self._seq.custom_channels["Counts"].emit(self._datay)
            self._seq.custom_channels["Time"].emit(self.self._calibx)

    def acq(self, acq_time=0, sleep_time=0.1, clear=False):
        self._acq_time = acq_time
        nb_channels = self._hwc.proxy.Channels
        self._datax = numpy.linspace(1, nb_channels, nb_channels)
        self._calibx = self.calibration.a * self._datax  + self.calibration.b
        self._plot_init()
        with cleanup(self._stop_acq):
            self._hwc.acq_time = acq_time
            self._hwc.proxy.ClearMemory()
            self._hwc.start()
            first_time = True
            while self._hwc.state().name == "ON":
                if not first_time:
                    print("\x1b[1A"*3)
                else:
                    first_time = False
                print(f"  Live Time (s): {self._hwc.proxy.LiveTime}")
                print(f"  Real Time (s): {self._hwc.proxy.RealTime}")
                self._datay = self._hwc.get_data()
                self._plot.set_data(Channels=self._datax, Counts=self._datay)
                gevent.sleep(sleep_time)

    """
    PLOT
    """
    def _plot_init(self):
        self._flint = flint()
        self._plot = self._flint.get_plot("curve", unique_name="Mca Canberra 340")
        self._plot.add_curve_item("Channels", "Counts", legend="mca_data", linewidth=1)


class Can556Calibration:

    def __init__(self, controller):
        self._controller = controller
        self._a = SimpleSetting(f"Can556Calibration_{self._controller._name}_a", default_value=1.0)
        self._b = SimpleSetting(f"Can556Calibration_{self._controller._name}_b", default_value=0.0)

    def __info__(self):
        message = f"Calibration for {self._controller._name}:\n"
        txt  = f"a: {self.a}\n"
        txt += f"b: {self.b}\n"
        message += txt
        return message

    @property
    def a(self):
        return self._a.get()

    @a.setter
    def a(self, value):
        self._a.set(value)

    @property
    def b(self):
        return self._b.get()

    @a.setter
    def b(self, value):
        self._b.set(value)

class Can556RoiContainer:

    def __init__(self, controller):
        self._controller = controller

        # ~ self._channels = self._controller.channels
        # comes back with error message "Can556HardwareController' object has no attribute 'proxy'"
        self._channels = 8192

        self._rois = {}
        self._roiss = HashSetting(f"Can556Roi_{self._controller.name}_roisdict", default_values={})
        _ = self._roiss.get_all()
        for name in _.keys():
            _itsalist = eval(_[name])
            self.add(name, _itsalist[0], _itsalist[1], _itsalist[2])

    def __info__(self):
        message = f"\nRois for {self._controller._name}:\n"
        txt = ""
        for name in self._rois.keys():
            txt += f"{name}: {self._rois[name]}\n"
        else:
            if txt == "":
                txt = "no rois"
        message += txt
        return message

    def delete_all(self):
        roikeys = self._rois.keys()
        for name in roikeys:
            self._rois[name].remove()
        self._rois = {}
        self._roiss.clear()

    def add(self, name, frm, to, active=True):
        self._rois[name] = Can556Roi(self._controller, name, frm, to, active)
        self._roiss.set(self._rois)
        self._set_roi_attr(name)

    def _remove_roi(self, name):
        self._del_roi_attr(self._rois[name])
        self._rois[name].remove()
        self._roiss.set(self._roi)

    def _set_roi_attr(self, roiname):
        setattr(self, roiname, self._rois[roiname])

    def _del_roi_attr(self, roiname):
        delattr(self, roiname)

    def menu(self):
        self._roimenu(self._channels)

    # the number 8192 should be self._channels, but "NameError: name 'self' is not defined"
    def _editroi(self, name, frm=0, to=1, active=True, channels=8192):
        if name is None:
            name = menu_word("Enter name")
        frm    = menu_number(f"Roi {name} from  :", minmax=[0,channels-1], default=frm,    integer=True)
        to     = menu_number(f"Roi {name} to    :", minmax=[1,channels],   default=to,     integer=True)
        active = menu_number(f"Roi {name} active:",                        default=active, integer=True)
        active = bool(active)
        return name, frm, to, active


    def _roimenu(self, channels=8192):
        namlist = list(self._rois.keys())
        namlist.append("add new roi")

        retval = menu_list(f"CAN {self._controller.name} ROI menu", namlist, random_text=self.__info__())
        if retval[1] == len(namlist)-1: # add with last option
            name = None
        elif retval[1] >= 0:
            name = retval[0]
        # ~ name, frm, to, active = self._editroi(name, self._rois[name].frm, self._rois[name].to, self._rois[name].active, channels)
        # no way to get frm and to from the object, right now. So just let them put new values.
        name, frm, to, active = self._editroi(name)
        self._rois[name] = Can556Roi(self._controller, name, frm, to, active)
        self._roiss.set(self._rois)

class Can556Roi():
    def __init__(self, controller, name, frm, to, active=True):

        self._controller = controller

        self._name = name

        self._frm = int(frm)
        self._to = int(to)

        self._from = frm
        self._to = to
        self._active = active

        self._check_validity(self._frm, self._to)

    def _check_validity(self, frm, to):
        if frm < 0:
            raise ValueError(f"Roi {self.name}: starting channel must be >= 0, not {frm}")
        if to <= 0:
            raise ValueError(f"Roi {self.name}: to must be > 0, not {to}")
        # unfortunately I can't get bliss to give me the controllers channel property :-(
        # ~ if self._frm > self._controller._hwc.proxy.channels:
            # ~ raise ValueError(f"Roi {self.name}: starting channel is bigger than number of channels")
        # ~ if self._frm + self._to > self._controller._hwc.proxy.channels:
            # ~ raise ValueError(f"Roi {self.name}: roi goes beyond number of channels")
        return True


    def __info__(self):
        return f"<{self._frm},{self._to}> {'Active' if self._active else 'Inactive'} "

    def __str__(self): # this is used by the parent class' _info_() to print.
        # ~ return f"str: <{self._frm},{self._to}> {'Active' if self._active else 'Inactive'} "
        return str([ self._frm, self._to, self._active ])

    def remove(self):
        # ~ self._roiss.clear()
        pass

    def modify(self, frm, to):
        self.add(frm, to)
        # ~ self._check_validity(frm, to)
        # ~ self._frm.set(frm)
        # ~ self._to.set(to)
        # ~ self._activate.set(True)

    def on(self):
        self._active.set(True)

    def off(self):
        self._active.set(False)

    def _calculate(self, spectrum, average=False):
        roi_value = 1
        for i in range(self._frm, self._to):
            roi_value += spectrum[i]
        if average:
            num = self._to - self._frm
            roi_value /= num
        return roi_value

